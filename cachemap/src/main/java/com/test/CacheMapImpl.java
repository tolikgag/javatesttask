package com.test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public class CacheMapImpl<KeyType, ValueType> implements CacheMap<KeyType, ValueType> {

    private long timeToLive;

    // return true if value is valid
    private Predicate<Entry> ttlPredicate = e -> Clock.getTime() - e.getTime() <= timeToLive;

    private Map<KeyType, Entry> map = new HashMap<KeyType, Entry>();


    public void setTimeToLive(long timeToLive) {
        this.timeToLive = timeToLive;
    }

    public long getTimeToLive() {
        return this.timeToLive;
    }

    public ValueType put(KeyType key, ValueType value) {
        Entry entry = map.put(key, new Entry(value, Clock.getTime()));
        if (entry != null && ttlPredicate.test(entry)) {
            return entry.getValue();
        }
        return null;
    }

    public void clearExpired() {
        map.entrySet().removeIf(e -> !ttlPredicate.test(e.getValue()));
    }

    public void clear() {
        map.clear();
    }

    public boolean containsKey(Object key) {
        Entry entry = map.get(key);
        return entry != null && ttlPredicate.test(entry);
    }

    public boolean containsValue(Object value) {
        return map.values().stream().
                filter(v -> v.getValue().equals(value) && ttlPredicate.test(v)).
                findAny().isPresent();
    }

    public ValueType get(Object key) {
        Entry entry = map.get(key);
        if (entry != null && ttlPredicate.test(entry)) {
            return entry.getValue();
        }
        return null;
    }

    public boolean isEmpty() {
        clearExpired();
        return map.isEmpty();
    }

    public ValueType remove(Object key) {
        Entry entry = map.remove(key);
        if (entry != null && ttlPredicate.test(entry)) {
            return entry.getValue();
        }
        return null;
    }

    public int size() {
        clearExpired();
        return map.size();
    }

    private class Entry {
        private ValueType value;
        private long time;

        Entry(ValueType value, long time) {
            this.value = value;
            this.time = time;
        }

        ValueType getValue() {
            return value;
        }

        long getTime() {
            return time;
        }
    }
}
