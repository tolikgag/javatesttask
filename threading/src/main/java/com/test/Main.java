package com.test;

public class Main {
    private static final int DEFAULT_VALUE = 40;
    private static final int DEFAULT_CALC_COUNT = 100;
    private static final int DEFAULT_THREADS = 10;


    public static void main(String[] args) throws InterruptedException {
        // get parameters
        final int n = args.length >=1 ? Integer.parseInt(args[0]) : DEFAULT_VALUE,
                calculationCount = args.length >=2 ? Integer.parseInt(args[1]) : DEFAULT_CALC_COUNT,
                threadPoolSize = args.length >=3 ? Integer.parseInt(args[2]) : DEFAULT_THREADS;

        // create runnable task
        Runnable task = () -> {
            FibCalc fibCalc = new FibCalcImpl();
            fibCalc.fib(n);
        };

        PerformanceTestResult result = new PerformanceTesterImpl().
                runPerformanceTest(task, calculationCount, threadPoolSize);

        System.out.println(result);
    }
}
