package com.test;

public class FibCalcImpl implements FibCalc {
    @Override
    public long fib(int n) {
        return fibRecursive(n);
    }

    private long fibRecursive(int n) {
        return n <= 1 ? n : fibRecursive(n-1) + fibRecursive(n-2);
    }

    // too fast, isn't suitable for this test
    private long fibNonRecursive(int n) {
        if(n <= 1){
            return n;
        }
        int fib = 1;
        int fibPrev = 1;
        for(int i = 2; i < n; i++){
            int temp = fib;
            fib += fibPrev;
            fibPrev = temp;
        }
        return fib;
    }
}
