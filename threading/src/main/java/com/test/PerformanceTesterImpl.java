package com.test;

import java.util.ArrayList;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class PerformanceTesterImpl implements PerformanceTester{
    @Override
    public PerformanceTestResult runPerformanceTest(Runnable task, int executionCount, int threadPoolSize)
            throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);

        // wrap by Callable to return time execution
        Callable<Long> callableTask = () -> {
            long time = System.currentTimeMillis();
            task.run();
            return System.currentTimeMillis() - time;
        };

        // add tasks to ExecutorService
        List<Future<Long>> futureList = new ArrayList<>(executionCount);
        for (int i=0; i < executionCount; i++) {
            futureList.add(executor.submit(callableTask));
        }

        // terminate and wait
        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.MINUTES);

        // get time executions
        List<Long> timeExecutionList = new ArrayList<>(executionCount);
        for (Future<Long> future : futureList) {
            try {
                timeExecutionList.add(future.get());
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        // generate statistics
        LongSummaryStatistics statistics  = timeExecutionList.stream().mapToLong(i -> i).summaryStatistics();

        return new PerformanceTestResult(statistics.getSum(),statistics.getMin(),statistics.getMax());
    }
}
