package com.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AddressDb {

	private static final Logger logger = LoggerFactory.getLogger(AddressDb.class);

	private static final String DB_URL = "jdbc:oracle:thin:@prod";
	private static final String DB_USER = "admin";
	private static final String DB_PASSWORD = "beefhead";

	private static final String INSERT_STAT = "insert into AddressEntry values (?, ?, ?)";
	private static final String GET_ALL_STAT = "select * from AddressEntry";
	private static final String GET_BY_NAME_STAT = "select * from AddressEntry where name = ?";

	static {
		try {
			Class.forName("oracle.jdbc.ThinDriver");
		} catch (ClassNotFoundException e) {
			logger.error("Driver isn't found", e);
		}
	}

	//FIXME Method isn't used, probably should be removed
	public void addPerson(Person person) {
		try(Connection conn = getConn();PreparedStatement statement = conn.prepareStatement(INSERT_STAT)) {
			statement.setLong(1, System.currentTimeMillis());
			statement.setString(2, person.getName());
			statement.setString(3, person.getPhoneNumber());
			statement.executeUpdate();
		} catch (SQLException e) {
			logger.error("Couldn't add Person to DB", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Looks up the given person, null if not found.
	 */
	public Person findPerson(String name) {
		try(Connection conn = getConn();PreparedStatement statement = conn.prepareStatement(GET_BY_NAME_STAT)) {
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return new Person(resultSet.getString("name"), resultSet.getString("phoneNumber"));
			}
		} catch (SQLException e) {
			logger.error("Couldn't find Person by name in DB", e);
		}
		return null;
	}

	public List<Person> getAll() {
		try(Connection conn = getConn();PreparedStatement statement = conn.prepareStatement(GET_ALL_STAT)) {
			ResultSet resultSet = statement.executeQuery();
			List<Person> entries = new LinkedList<>();
			while (resultSet.next()) {
				entries.add(new Person(resultSet.getString("name"), resultSet.getString("phoneNumber")));
			}
			return entries;
		} catch (SQLException e) {
			logger.error("Couldn't get Persons from DB", e);
			return Collections.emptyList();
		}
	}

	private Connection getConn() throws SQLException {
		return DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
	}
}
