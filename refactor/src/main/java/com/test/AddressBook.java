package com.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class AddressBook {

	private AddressDb db = new AddressDb();

	static {
		new Checker().start();
	}

	public boolean hasMobile(String name) {
		return getMobile(name) != null;
	}

	public int getSize() {
		return db.getAll().size();
	}

	/**
	 * Get the given user's mobile phone number,
	 * or null if he doesn't have one.
	 */
	public String getMobile(String name) {
		Person person = db.findPerson(name);
		if(person != null && person.getPhoneNumber().startsWith("070")) {
			return person.getPhoneNumber();
		}
		return null;
	}

	/**
	 * Returns all names of the people. Truncates to the given length.
	 */
	//FIXME I'd like to return distinct names
	public List getNames(int maxLength) {
		return db.getAll().stream().
				map(p -> p.getName().length() > maxLength ? p.getName().substring(0, maxLength) : p.getName()).
				collect(Collectors.toList());
	}

	/**
	 * Returns all people who have mobile phone numbers.
	 */
	//FIXME I'd like to rename this method's name and specify return type through generic List<T> or List<Person>
	public List getList() {
		//FIXME I'd like to create specific method in AddressDb
		return db.getAll().stream().
				filter(p -> p.getPhoneNumber().startsWith("070")).
				collect(Collectors.toList());
	}

	//FIXME "while (System.currentTimeMillis() < time)" I think it's a bug or redundant code.
	//FIXME I decided to fix rather than clean
	private static class Checker extends Thread {
		private static final Logger logger = LoggerFactory.getLogger(Checker.class);

		private static AddressBook addressBook = new AddressBook();

		public void run() {
			while (true) {
				addressBook.getList();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.error("Checker thread was interrupted");
				}
			}

		}
	}

}
